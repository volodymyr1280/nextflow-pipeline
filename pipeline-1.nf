log.info """
    My first Next Flow pipeline
    ===========================
    input_dir  : ${params.input_dir}
    output_dir : ${params.output_dir}
""".stripIndent()

// Separate process per file path https://github.com/nextflow-io/patterns/blob/master/docs/process-per-file-path.adoc
Channel.fromPath("${params.input_dir}/*.doc").set{ input_ch }

process convertDocToPdf {
    // container 'job-definition://xxx'
    container 'volodymyr128/doc-converter'
    cpus 2
    disk '20 GB'
    echo true
    memory '2 GB'
    publishDir params.output_dir

    input:
    file x from input_ch

    output:
    file '*.pdf' into pdf_channel

    """
        java -jar /usr/local/converter.jar -i "${x}" -o "${x}.pdf" -t "DOC"
    """
}

// Process per each output file https://github.com/nextflow-io/patterns/blob/master/docs/process-per-file-output.adoc
process getSize {
    container 'ubuntu:18.04'
    cpus 1
    disk '10 GB'
    echo true
    memory '2 GB'

    input:
    file pdf_file from pdf_channel.flatten()

    output:
    stdout receiver

    """
        echo `wc -c < $pdf_file`
    """
}

receiver.view { "Received: $it" }

workflow.onComplete {
	println ( workflow.success ? "Done" : "Oops .. something went wrong" )
}
