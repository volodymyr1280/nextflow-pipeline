log.info """
    The Next Flow head process
    ===========================
""".stripIndent()


nfOpts = "--input_dir ${params.input_dir} --output_dir ${params.output_dir}"

process head {
    queue = 'workflow-queue'
    container 'volodymyr128/nextflow-head'
    errorStrategy 'terminate'
    time '12h'
    cpus 1
    disk '10 GB'
    memory '512 MB'

    """
        PIPELINE_URL=${params.pipe_url} NF_SCRIPT=${params.pipe_script} NF_OPTS="${nfOpts}" /usr/local/bin/entrypoint.sh
    """
}
